# Changing Project name

# install the necessary package
brew install rename ack

# run this for a couple of times
find . -name 'NewProject*' -print0 | xargs -0 rename -S 'NewProject' 'NewProject'

ack --literal --files-with-matches 'NewProject' | xargs sed -i '' 's/NewProject/NewProject/g'

# double confirm, if no output, that means success
ack --literal 'NewProject'
