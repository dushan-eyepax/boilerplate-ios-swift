//
//  InternalNotification.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation


protocol InternalNotificationSenderDelegate {
    func postInternalNotification(name: Notification.Name, object: Any?, userInfo: [AnyHashable : Any]?)
}

extension InternalNotificationSenderDelegate {
    
    func postInternalNotification(name: Notification.Name, object: Any?, userInfo: [AnyHashable : Any]? = nil) {
        NotificationCenter.default.post(name: name, object: object, userInfo: userInfo)
    }
}
