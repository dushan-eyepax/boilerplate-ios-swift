//
//  DeviceDetails.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit

protocol DeviceDetails {
    var deviceDetailsDictionary: [String: Any] { get }
}

extension DeviceDetails {
    var deviceDetailsDictionary: [String: Any] {
        let dic: [String: String] = [
            "device_id": UIDevice.current.identifierForVendor!.uuidString,
            "device_type": "ios",
            "device_push_token": AppUserDefault.getFCMToken()
        ]
        return dic
    }
}
