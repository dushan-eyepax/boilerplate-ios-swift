//
//  LocationManager.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa

let LM = LocationManager.self

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    private var manager: CLLocationManager!
    
    var userLocation: BehaviorRelay<CLLocation?> = BehaviorRelay<CLLocation?>(value: nil)
    
    private override init() {
        super.init()
        configure()
    }
    
    func configure() {
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
    }
    
    func determineCurrentLocation() {
        if CLLocationManager.locationServicesEnabled() {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let _userLocation: CLLocation = locations[0] as CLLocation
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
         userLocation.accept(_userLocation)
         manager.stopUpdatingLocation()        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        log.error(error.localizedDescription)
    }
}
