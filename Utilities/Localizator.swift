//
//  Localizator.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit

public class Localizator {
    fileprivate func NSLocalizedString(_ key: String) -> String {
        return Foundation.NSLocalizedString(key, comment: "")
    }
}

extension String {
    
    // Side menu segue identifiers
    static let SideMenuToMenu = NSLocalizedString("SideMenu.Menu", comment: "")
    static let SideMenuToDashboard = NSLocalizedString("SideMenu.Dashboard", comment: "")
    
    
    // Segue Identifiers
    
    
    // Unwind Segue Identifiers
    
    
    // UIImage Identifiers
    
    
    // Loding messages
    static let CreatingAccount = NSLocalizedString("Creating Account...", comment: "")
    static let Loading = NSLocalizedString("Loading...", comment: "")
    static let Login = NSLocalizedString("Login...", comment: "")
    static let Logout = NSLocalizedString("Logout...", comment: "")
    
    // File extensions
    static let PDF = NSLocalizedString("pdf", comment: "")
    static let HTML = NSLocalizedString("html", comment: "")
    static let DOCX = NSLocalizedString("docx", comment: "")
    static let PNG = NSLocalizedString("png", comment: "")
    
    // TableViewCells
    static let NewsCell = NSLocalizedString("NewsCell", comment: "")
    
    
    // CollectionViewCells
    static let TutorialCell = NSLocalizedString("TutorialCell", comment: "")
    
    // Alert titles
    static let Alert = NSLocalizedString("Alert", comment: "")
    static let Confirmation = NSLocalizedString("Confirmation", comment: "")
    static let Error = NSLocalizedString("Error", comment: "")
    static let Incomplete = NSLocalizedString("Incomplete", comment: "")
    static let Success = NSLocalizedString("Success", comment: "")
    static let Failed = NSLocalizedString("Failed", comment: "")
    static let Oops = NSLocalizedString("Oops", comment: "")
    static let Warning = NSLocalizedString("Warning", comment: "")
    
    // Alert Messages
    // Info messages
    static let FeatureNotImplemented = NSLocalizedString("This feature is not implemented.", comment: "")
    static let FeatureComingSoon = NSLocalizedString("This feature is coming soon.", comment: "")
    static let SomethingWentWrong = NSLocalizedString("Something went wrong.", comment: "")
    
    // Error messages
    static let ErrorCorrupted = NSLocalizedString("Error is corrupted.", comment: "")
    static let MissingData = NSLocalizedString("Missing data in the request.", comment: "")
    static let AnyFieldEmpty = NSLocalizedString("All fields are required.", comment: "")
    static let MissingRequiredFields = NSLocalizedString("Missing some required fields.", comment: "")
    static let FirstNameEmpty = NSLocalizedString("Please enter first name.", comment: "")
    static let LastNameEmpty = NSLocalizedString("Please enter last name.", comment: "")
    static let AnyPasswordEmpty = NSLocalizedString("Please fill both password fields.", comment: "")
    static let EmailEmpty = NSLocalizedString("Please enter email address.", comment: "")
    static let CurrentEmailEmpty = NSLocalizedString("Please enter current email address.", comment: "")
    static let NewEmailEmpty = NSLocalizedString("Please enter new email address.", comment: "")
    static let PasswordEmpty = NSLocalizedString("Please enter password.", comment: "")
    static let PhoneEmpty = NSLocalizedString("Please enter phone number.", comment: "")
    static let CountryCodeEmpty = NSLocalizedString("Please select country code.", comment: "")
    static let InvalidEmail = NSLocalizedString("Invalid email address.", comment: "")
    static let InvalidCurrentEmail = NSLocalizedString("Invalid current email address.", comment: "")
    static let InvalidNewEmail = NSLocalizedString("Invalid new email address.", comment: "")
    static let IncorrectCurrentEmail = NSLocalizedString("Incorrect current email address.", comment: "")
    static let InvalidPhone = NSLocalizedString("Invalid phone number.", comment: "")
    static let InvalidUrl = NSLocalizedString("Invalid url.", comment: "")
    static let InvalidPassword = NSLocalizedString("Invalid password.", comment: "")
    static let InvalidCurrentPassword = NSLocalizedString("Invalid current password.", comment: "")
    static let ShorterPassword = NSLocalizedString("Password should be at least \(Constants.Counts.passwordCount) characters.", comment: "")
    static let ShorterNewPassword = NSLocalizedString("New Password should be at least \(Constants.Counts.passwordCount) characters.", comment: "")
    static let MismatchingPasswords = NSLocalizedString("Passwords do not match.", comment: "")
    static let MismatchingNewPasswords = NSLocalizedString("New Passwords do not match.", comment: "")
    static let CurrentPasswordEmpty = NSLocalizedString("Please enter current password.", comment: "")
    static let NewPasswordEmpty = NSLocalizedString("Please enter new password.", comment: "")
    static let ConfirmPasswordEmpty = NSLocalizedString("Please enter confirm password.", comment: "")
    static let ConfirmNewPasswordEmpty = NSLocalizedString("Please enter confirm new password.", comment: "")
    static let newCurrentMatchPasswords = NSLocalizedString("Current password and new password cann't be same.", comment: "")
    static let OTPCodeEmpty = NSLocalizedString("Please enter OTP code.", comment: "")
    static let OTPCodeInvalid = NSLocalizedString("Invalid OTP code.", comment: "")
    static let InternetConnectionOffline = NSLocalizedString("Internet connection appears to be offline. ", comment: "")
    static let MessageEmpty = NSLocalizedString("Please type your message.", comment: "")
    static let UserLocationNotFound = NSLocalizedString("Unable to find user location. Please try again.", comment: "")
    static let NoCameraPermission = NSLocalizedString("No camera permission set.", comment: "")
    
    // Success messages
    static let CreateAccountSuccess = NSLocalizedString("Account has been created successfully.", comment: "")
    
    // Failure messages
    static let CreateAccountFailed = NSLocalizedString("Failed to create account.", comment: "")
    static let LoadDataFailed = NSLocalizedString("Failed to load data.", comment: "")
    
    // No data messages
    
    
    // Info messages
    
    // Confirmation messages
    static let LogoutConfirmation = NSLocalizedString("Are you sure you want to logout?", comment: "")
    
    // Action titles
    static let OK = NSLocalizedString("OK", comment: "")
    static let Cancel = NSLocalizedString("Cancel", comment: "")
    static let Yes = NSLocalizedString("Yes", comment: "")
    static let No = NSLocalizedString("No", comment: "")
    static let Retry = NSLocalizedString("Retry", comment: "")
    static let Skip = NSLocalizedString("Skip", comment: "")
    static let Dismiss = NSLocalizedString("Dismiss", comment: "")
    static let TakePhoto = NSLocalizedString("Take Photo", comment: "")
    static let TakeVideo = NSLocalizedString("Take Video", comment: "")
    static let ChooseFromLibrary = NSLocalizedString("Choose from Library", comment: "")
    static let Call = NSLocalizedString("Call", comment: "")
    static let Chat = NSLocalizedString("Chat", comment: "")
    static let Notification = NSLocalizedString("Notification", comment: "")
    static let Delete = NSLocalizedString("Delete", comment: "")
    static let Settings = NSLocalizedString("Settings", comment: "")
    
    // Placeholders
    
    
    // Other
    static let NotApplicable = NSLocalizedString("N/A", comment: "")
    static let LineBreak = NSLocalizedString("\n", comment: "")
    static let Comma = NSLocalizedString(",", comment: "")
    static let CommaWithSpace = NSLocalizedString(", ", comment: "")
    static let QuestionMark = NSLocalizedString("?", comment: "")
    static let SingleQotation = NSLocalizedString("'", comment: "")
    static let EmptyString = NSLocalizedString("", comment: "")
    
    
    // Top view controller of the Storyboards
    static let authNC = NSLocalizedString("AuthNC", comment: "")
    static let homeNC = NSLocalizedString("HomeNC", comment: "")
    static let sideMenuConfigurationVC = NSLocalizedString("SideMenuConfigurationVC", comment: "")
    static let mainTBC = NSLocalizedString("MainTBC", comment: "")
    
    // View Controllers
    static let LoginVC = NSLocalizedString("LoginVC", comment: "")
    static let RegisterVC = NSLocalizedString("RegisterVC", comment: "")
    static let RetrievePasswordVC = NSLocalizedString("RetrievePasswordVC", comment: "")
    static let ChangePasswordVC = NSLocalizedString("ChangePasswordVC", comment: "")
    static let ChangeEmailVC = NSLocalizedString("ChangeEmailVC", comment: "")
    static let AppDetailsVC = NSLocalizedString("AppDetailsVC", comment: "")
    
    // Navigation bar titles
    
    
    // Error messages with API
    
}

// Colors
extension UIColor {
    static let appThemeDarkBlue = UIColor(red: 0.05, green: 0.12, blue: 0.21, alpha: 1.0)
    static let appThemeOrrange = UIColor.systemOrange
    
}

// Icons
extension UIImage {
    
}

extension Notification.Name {
    static let didUpdateProfileImage = NSNotification.Name(rawValue: "didUpdateProfileImage")
}
