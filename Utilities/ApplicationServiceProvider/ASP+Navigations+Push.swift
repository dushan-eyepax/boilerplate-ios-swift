//
//  ASP+Navigations+Push.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

extension ApplicationServiceProvider {

    //MARK: Navigation push view Controller
    public func pushToViewController(in sb: Storyboard, for identifier: String, from vc: UIViewController?, data: Any? = nil) {
        
        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: identifier)
        
        /*if destVc is YourViewCntroller {
            let _vc = storyboard.instantiateViewController(withIdentifier: identifier) as! YourViewCntroller
            _vc.data = _data
            
            vc?.navigationController?.pushViewController(_vc, animated: true)
        }
        
        else if destVc is YourOtherViewCntroller {
            
        }
        
        else {
            vc?.navigationController?.pushViewController(destVc, animated: true)
        }*/
    }
    
}
