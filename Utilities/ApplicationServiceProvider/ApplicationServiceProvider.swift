//
//  ApplicationService.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit
import SwiftyBeaver
import RealmSwift

let log = SwiftyBeaver.self
let ASP = ApplicationServiceProvider.self

enum Storyboard: String {
    case Auth
    case Main
    case SideMenu
    case TabBar
}

class ApplicationServiceProvider {
    
    static let shared = ApplicationServiceProvider()
    
    let bundleId = Bundle.main.bundleIdentifier ?? ""
    let deviceId = UIDevice.current.identifierForVendor!.uuidString
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    let deviceType = "APPLE"
    
    // Initial parameters for authenication services (Login / Register)
    var initialAuthParameters: [String: Any] = [:]
    
    public init() {}
    
    // Configure Application
    func configure() {
        
        setInitialAuthParameters()
        
        manageUIAppearance()
        
        configureLogger()
        
        printRealmPath()
    }
    
    //MARK: Setup initial auth parameters
    private func setInitialAuthParameters() {
        let dict = [
            "device_id": ASP.shared.deviceId,
            "device_type": ASP.shared.deviceType,
            "device_push_token": AppUserDefault.getFCMToken()
        ]
        
        initialAuthParameters.updateDictionary(otherValues: dict)
    }
    
    //MARK: Setup application appearance
    private func manageUIAppearance(hasCustomBackButton: Bool = false, hideNavBarShadow: Bool = false, hideTabBarShadow: Bool = false) {
        // Set navigation bar tint / background color
        UINavigationBar.appearance().isTranslucent = false
        
        // Set navigation bar item tint color
        UIBarButtonItem.appearance().tintColor = .darkGray
        
        // Set navigation bar back button tint color
        UINavigationBar.appearance().tintColor = .darkGray
        
        // Set cutom back image if needed
        if hasCustomBackButton == true {
            // Set back button image
            let backImage = #imageLiteral(resourceName: "ic_back")
            UINavigationBar.appearance().backIndicatorImage = backImage
            UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        }
        
        // Hide navigation bar shadow if needed
        if hideNavBarShadow == true {
            // To remove the 1px seperator at the bottom of navigation bar
            UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
            UINavigationBar.appearance().shadowImage = UIImage()
        }
        
        // Hide tab bar shadow if needed
        if hideTabBarShadow == true {
            // To remove the 1px seperator at the bottom of tab bar
            UITabBar.appearance().backgroundImage = UIImage()
            UITabBar.appearance().shadowImage = UIImage()
        }
    }
    
    
    //MARK: Logger
    private func configureLogger() {
        let console = ConsoleDestination() // log to Xcode Console
        log.addDestination(console)
    }
    
    
    //MARK: Print Realm path
    private func printRealmPath() {
        print("Realm path: \(String(describing: try! Realm().configuration.fileURL))")
    }
    
}
