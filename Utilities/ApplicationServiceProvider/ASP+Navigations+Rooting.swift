//
//  ASP+Navigations+Rooting.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

extension ApplicationServiceProvider {

    //MARK: Manage User Direction
    public func manageUserDirection(from vc: UIViewController? = nil, window: UIWindow? = nil) {
        guard LocalUser.current() != nil else {
            directToPath(in: .Auth, for: .authNC, from: vc, window: window)
            return
        }
        
        redirectionWithMainInterfaceType(type: ApplicationControl.appMainInterfaceType, window: window)
    }
    
    
    //MARK: Get ridirection with app main interface type
    public func redirectionWithMainInterfaceType(type: MainInterfaceType, from vc: UIViewController? = nil, window: UIWindow? = nil) {
        switch type {
        case .Main:
            directToPath(in: .Main, for: .homeNC, from: vc, window: window)
            
        case .SideMenuNavigations:
            directToPath(in: .SideMenu, for: .sideMenuConfigurationVC, from: vc, window: window)
            
        case .TabBarNavigations:
            directToPath(in: .TabBar, for: .mainTBC, from: vc, window: window)
            
        case .Custom:
            break
        }
    }
    
    
    //MARK: Direct to Main Root window
    public func directToPath(in sb: Storyboard, for identifier: String, from vc: UIViewController? = nil, window: UIWindow? = nil) {
        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
        let topController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        // Getting access to the window object from SceneDelegate
        if #available(iOS 13.0, *) {
            guard let sceneDelegate: SceneDelegate = UIApplication.shared.connectedScenes
                .first?.delegate as? SceneDelegate else {
                    return
            }
            sceneDelegate.window?.rootViewController = topController
            
        } else {
            // Fallback on earlier versions
            appDelegate.setAsRoot(_controller: topController)
        }
    }
    
    //MARK: Get ViewController (VC, TVC, CVC, PC, TBC etc.)
    public func viewController(in sb: Storyboard, identifier: String) -> UIViewController {
        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
        let targetVC = storyboard.instantiateViewController(withIdentifier: identifier)
        return targetVC
    }
    
}
