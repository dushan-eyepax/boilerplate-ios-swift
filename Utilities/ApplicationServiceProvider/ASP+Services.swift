//
//  ASP+Services.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

extension ApplicationServiceProvider {

    //MARK: Clear image cache
    public func clearImageCache(url: URL) {
        // Clear from cache
        let urlRequest = URLRequest(url: url)
        let imageDownloader = UIImageView.af.sharedImageDownloader
        _ = imageDownloader.imageCache?.removeImage(for: urlRequest, withIdentifier: nil)
        imageDownloader.session.sessionConfiguration.urlCache?.removeCachedResponse(for: urlRequest)
    }

    //MARK: Get url query from array of items (Int, String,Double etc. arrays)
    public func getQueryFromArray(key: String, array: [Any]) -> String? {
        
        var query = ""
        
        guard !(array.isEmpty) else {
            return nil
        }
        
        for i in 0...(array.count - 1) {
            query.append("\(key)[\(i)]=\(array[i])&")
        }
        
        query.removeLast()
        
        return query
        
    }
    
    //MARK: Get url query from dictionary
    public func getQueryFromDictionary(dict: [String: Any]) -> String? {
        
        var query = ""
        
        guard !(dict.isEmpty) else {
            return nil
        }
        
        for (key, value) in dict {
            query.append("\(key)=\(value)&")
        }
        
        query.removeLast()
        
        return query
        
    }
    
}

func delay(_ delay: Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
