//
//  ASP+Navigations+Present.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

extension ApplicationServiceProvider {

    //MARK: Present view Controller
    public func presentViewController(in sb: Storyboard, for identifier: String, from vc: UIViewController?, style: UIModalPresentationStyle = .overCurrentContext, data: Any? = nil) {
        
        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: identifier)
        
        destVc.modalPresentationStyle = style
        
        /*if destVc is YourViewCntroller {
            let _vc = storyboard.instantiateViewController(withIdentifier: identifier) as! YourViewCntroller
            _vc.data = _data
            
            vc?.present(_vc, animated: true, completion: nil)
        }
        
        else if destVc is YourViewCntroller {
            
        }
        
        else {
            vc?.present(destVc, animated: true, completion: nil)
        }*/
    }
    
}
