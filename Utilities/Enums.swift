//
//  Enums.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation

enum ValidaterError: Error {
    case invalidData(String)
}

enum AppPermisionType: String {
    case Camera, Location, Contacts, Microphone
    case PhotoLibrary = "Photo Library"
}

enum ChangePasswordType {
    case OnlyNewPassword
    case CurrentPasswordAndNewPassword
    case OTPAndNewPassword
}
