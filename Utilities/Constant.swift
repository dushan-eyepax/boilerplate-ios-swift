//
//  Constants.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    static let iOSAppDownloadLink = ""
    
    enum ServerAPIKeys {
        static let googleServices = ""
        static let firebaseServices = ""
    }
    
    enum Counts {
        static let passwordCount = 6
        static let nameMinimumCharCount = 2
        static let otpCodeCount = 4
    }
    
    enum AppDetails {
        static let termsAndConditionsUrl = "https://"
        static let privacyPolicyUrl = "https://www"
        static let aboutUsUrl = "https://www."
    }
}
