//
//  AppUserDefault.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation

struct AppUserDefault {
    
    static let shared = UserDefaults.standard
    
    //MARK: FCM token
    /// Set FCM Token
    static func setFCMToken(token: String) {
        shared.set(token, forKey: "FCM_TOKEN")
        shared.synchronize()
    }
    
    /// Get FCM Token
    static func getFCMToken() -> String {
        if let token = shared.string(forKey: "FCM_TOKEN") {
            return token
        }
        return ""
    }
}
