//
//  CustomUIImageView.swift
//  PhoneMed
//
//  Created by Dushan Saputhanthri on 6/9/17.
//  Copyright © 2017 Dushan Saputhanthri. All rights reserved.
//

import UIKit

@IBDesignable class CustomUIImageView: UIImageView {

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
    }
}
