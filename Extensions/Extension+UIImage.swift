//
//  Extension+UIImage.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func tintedImage(color: UIColor) -> UIImage? {
        let templateImage = self.withRenderingMode(.alwaysTemplate)
        
        let imageView = UIImageView()
        imageView.image = templateImage
        imageView.tintColor = color
        
        return imageView.image
    }
    
}
