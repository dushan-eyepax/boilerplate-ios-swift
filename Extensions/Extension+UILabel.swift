//
//  Extension+UILabel.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    var optimalHeight:CGFloat {
        get {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byTruncatingTail
            label.font = self.font.withSize(15.0)
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
        }
    }
    
}
