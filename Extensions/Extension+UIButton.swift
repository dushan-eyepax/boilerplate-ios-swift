//
//  Extension+UIButton.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

extension UIButton {
    
    func setImageWithUrl(_ urlString: String, isBackground: Bool = false, state: UIControl.State = .normal, placeholderImage: UIImage = UIImage()) {
        if let url = URL(string: urlString) {
            switch isBackground {
            case true:
                self.af.setBackgroundImage(for: state, url: url, placeholderImage: placeholderImage, progressQueue: .global())
            default:
                self.af.setImage(for: state, url: url, placeholderImage: placeholderImage, progressQueue: .global())
            }
        }
    }
    
}
