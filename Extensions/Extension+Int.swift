//
//  Extension+Int.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation

extension Int {
    
    var roundedWithAbbreviations: String {
        
        let number = Double(self)
        let thousand = number / 1000
        let million = number / 1000000
        let billion = number / 1000000000
        
        if billion >= 1.0 {
            return "\(round(billion*10)/10)B"
        }
        else if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
    
}
