//
//  Extension+Dictionary.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import Foundation

extension Dictionary {
    
    mutating func updateDictionary(otherValues: Dictionary) {
        for (key, value) in otherValues {
            self.updateValue(value, forKey: key)
        }
    }
    
}
