//
//  BaseVC.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, LoadingIndicatorDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        deinitVC()
    }
    
    // Hide the keyboard when tap background
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Back navigation
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    // Dismiss Top VC
    func dismissTop() {
        dismiss(animated: true, completion: nil)
    }
    
    // Show Temp alert
    func showTempAlert(vc: UIViewController? = nil, tvc: UITableViewController? = nil) {
        if vc != nil {
            AlertProvider(vc: vc!).showAlert(title: .Alert, message: .FeatureNotImplemented, action: AlertAction(title: .Dismiss))
        } else if tvc != nil {
            AlertProvider(tvc: tvc!).showAlert(title: .Alert, message: .FeatureNotImplemented, action: AlertAction(title: .Dismiss))
        }
    }
    
    func setDelegatesForTextEditors(tfs: [UITextField] = [], tvs: [UITextView] = []) {
        tfs.forEach({ tf in
            tf.delegate = self
        })
        
        tvs.forEach({ tv in
            tv.delegate = self
        })
    }
    
    // Set TableView contet offset to zero
    func setTableViewContentOffset(_ tableView: UITableView, dataCount: Int) {
        guard dataCount > 0 else {
            return
        }
        
        tableView.layoutIfNeeded()
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
        tableView.setContentOffset(.zero, animated: false)
    }
    
}
