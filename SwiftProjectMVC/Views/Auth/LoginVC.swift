//
//  LoginVC.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

class LoginVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func didTapOnLogin(_ sender: UIButton) {
        
        ASP.shared.directToPath(in: .Main, for: .mainTBC, from: self)
    }
}
