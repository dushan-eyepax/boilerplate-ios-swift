//
//  HomeVC.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

class HomeVC: BaseVC {
    
    var allCountries: [Country] = []
    
    var country: Country?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get all countries
        getAllCountries()
        
        // Get selected country
        //getCountry(name: "Australia")
    }
    
}

//MARK: Network Services
extension HomeVC {
    
    func getAllCountries() {
        
        self.startLoading()
        CountryAPI.gerAllCountries(parameters: [:], completion: { data, error in
            self.stopLoading()
            
            guard error == nil,
                  let countries = data else {
                AlertProvider(vc: self).showAlert(title: .Error, message: .LoadDataFailed, action: AlertAction(title: .Dismiss))
                return
            }
            
            self.allCountries = countries
            
            AlertProvider(vc: self).showAlert(title: .Success, message: "You got \(countries.count) countries in the result.", action: AlertAction(title: .Dismiss))
            
            log.debug(countries)
        })
    }
    
    func getCountry(name: String) {
        
        self.startLoading()
        CountryAPI.gerCountry(name: name, completion: { data, error in
            self.stopLoading()
            
            guard error == nil,
                  let countries = data else {
                AlertProvider(vc: self).showAlert(title: .Error, message: .LoadDataFailed, action: AlertAction(title: .Dismiss))
                return
            }
            
            self.country = countries.first
            
            AlertProvider(vc: self).showAlert(title: .Success, message: "You got the country \(self.country?.name ?? "Unknown").", action: AlertAction(title: .Dismiss))
            
            log.debug(countries)
        })
    }
    
}
