//
//  ProfileVC.swift
//  Copyright © 2021 Eyepax. All rights reserved.
//

import UIKit

class ProfileVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func didTapOnLogout(_ sender: UIBarButtonItem) {
        
        ASP.shared.manageUserDirection()
    }
    
}
